Résumé
=======

Cet ensemble de données (ml-latest-small) décrit l'activité de notation 5 étoiles et de marquage de texte libre de [MovieLens](http://movielens.org), un service de recommandation de films. Il contient 100 836 classements et 3 683 applications de balises sur 9 742 films. Ces données ont été créées par 610 utilisateurs entre le 29 mars 1996 et le 24 septembre 2018. Cet ensemble de données a été généré le 26 septembre 2018.

Les utilisateurs ont été sélectionnés au hasard pour être inclus. Tous les utilisateurs sélectionnés avaient noté au moins 20 films. Aucune information démographique n’est incluse. Chaque utilisateur est représenté par un identifiant et aucune autre information n'est fournie.

Les données sont contenues dans les fichiers `links.csv`, `movies.csv`, `ratings.csv` et `tags.csv`. Plus de détails sur le contenu et l'utilisation de tous ces fichiers suivent.

Il s'agit d'un ensemble de données de *développement*. En tant que tel, il peut changer au fil du temps et ne constitue pas un ensemble de données approprié pour les résultats de recherche partagés. Consultez les ensembles de données *de référence* disponibles si telle est votre intention.

Cet ensemble de données ainsi que d'autres ensembles de données GroupLens sont accessibles au public en téléchargement sur <http://grouplens.org/datasets/>.



Identifiants utilisateur
---------

Les utilisateurs de MovieLens ont été sélectionnés au hasard pour être inclus. Leurs identifiants ont été anonymisés. Les identifiants d'utilisateur sont cohérents entre « ratings.csv » et « tags.csv » (c'est-à-dire que le même identifiant fait référence au même utilisateur dans les deux fichiers).


Identifiants de films
---------

Seuls les films avec au moins une note ou une balise sont inclus dans l'ensemble de données. Ces identifiants de films sont cohérents avec ceux utilisés sur le site Web MovieLens (par exemple, l'identifiant `1` correspond à l'URL <https://movielens.org/movies/1>). Les identifiants de films sont cohérents entre `ratings.csv`, `tags.csv`, `movies.csv` et `links.csv` (c'est-à-dire que le même identifiant fait référence au même film dans ces quatre fichiers de données).


Structure du fichier de données de notation (ratings.csv)
-----------------------------------------

Toutes les notes sont contenues dans le fichier « ratings.csv ». Chaque ligne de ce fichier après la ligne d'en-tête représente une évaluation d'un film par un utilisateur et a le format suivant :

     userId, movieId, note, horodatage

Les lignes de ce fichier sont classées d'abord par userId, puis, au sein de l'utilisateur, par movieId.

Les notes sont établies sur une échelle de 5 étoiles, avec des incréments d'une demi-étoile (0,5 étoiles - 5,0 étoiles).

Les horodatages représentent les secondes depuis minuit, temps universel coordonné (UTC), le 1er janvier 1970.

Tags Data File Structure (tags.csv)
-----------------------------------

Toutes les balises sont contenues dans le fichier `tags.csv`. Chaque ligne de ce fichier après la ligne d'en-tête représente une balise appliquée à un film par un utilisateur et a le format suivant :

     ID utilisateur, ID film, balise, horodatage

Les lignes de ce fichier sont classées d'abord par userId, puis, au sein de l'utilisateur, par movieId.

Les balises sont des métadonnées générées par l'utilisateur sur les films. Chaque balise est généralement un seul mot ou une courte phrase. La signification, la valeur et le but d'une balise particulière sont déterminés par chaque utilisateur.

Les horodatages représentent les secondes depuis minuit, temps universel coordonné (UTC), le 1er janvier 1970.


Structure des fichiers de données de films (movies.csv)
---------------------------------------

Les informations sur le film sont contenues dans le fichier « movies.csv ». Chaque ligne de ce fichier après la ligne d'en-tête représente un film et a le format suivant :

     ID du film, titre, genres

Les titres de films sont saisis manuellement ou importés depuis <https://www.themoviedb.org/> et incluent l'année de sortie entre parenthèses. Des erreurs et des incohérences peuvent exister dans ces titres.

Les genres sont une liste séparée par des barres et sont sélectionnés parmi les éléments suivants :

* Action
* Aventure
*Animations
* Enfants
* Comédie
* Crime
* Documentaire
* Drame
* Fantaisie
* Film noir
* Horreur
* Musicale
* Mystère
* Romance
* Science-fiction
* Thriller
* Guerre
* Occidental
* (aucun genre répertorié)


Structure du fichier de données des liens (links.csv)
---------------------------------------

Les identifiants qui peuvent être utilisés pour établir un lien vers d'autres sources de données de films sont contenus dans le fichier « links.csv ». Chaque ligne de ce fichier après la ligne d'en-tête représente un film et a le format suivant :

     ID du film, ID imdb, ID tmdb

movieId est un identifiant pour les films utilisé par <https://movielens.org>. Par exemple, le film Toy Story a le lien <https://movielens.org/movies/1>.

imdbId est un identifiant pour les films utilisé par <http://www.imdb.com>. Par exemple, le film Toy Story a le lien <http://www.imdb.com/title/tt0114709/>.

tmdbId est un identifiant pour les films utilisé par <https://www.themoviedb.org>. Par exemple, le film Toy Story a le lien <https://www.themoviedb.org/movie/862>.

L'utilisation des ressources répertoriées ci-dessus est soumise aux conditions de chaque fournisseur.


Validation croisée
----------------

Les versions antérieures de l'ensemble de données MovieLens incluaient soit des plis croisés précalculés, soit des scripts pour effectuer ce calcul. Nous ne regroupons plus aucune de ces fonctionnalités avec l'ensemble de données, car la plupart des boîtes à outils modernes la proposent en tant que fonctionnalité intégrée. Si vous souhaitez en savoir plus sur les approches standard du calcul croisé dans le contexte de l'évaluation des systèmes de recommandation, consultez [LensKit](http://lenskit.org) pour obtenir des outils, de la documentation et des exemples de code open source.